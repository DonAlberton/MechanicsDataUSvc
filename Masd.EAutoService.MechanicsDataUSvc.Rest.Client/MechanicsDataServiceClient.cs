﻿using System.Net.Http;
using System.Text.Json;
using Masd.EAutoService.MechanicsDataUSvc.Rest.Model;

namespace Masd.EAutoService.MechanicsDataUSvc.Rest.Client
{

    public class MechanicsDataServiceClient: IMechanicsDataService
    {
        private static readonly HttpClient httpClient = new HttpClient();
        private static int port = 8081;
        private static string protocol = "http";
        public MechanicDTO[] GetMechanics()
        {
            string webServiceUrl = $"{protocol}://localhost:{port}/MechanicsData/GetMechanics";

            Task<string> webServiceCall = CallWebService(HttpMethod.Get, webServiceUrl);

            webServiceCall.Wait();

            string jsonResponseContent = webServiceCall.Result;

            MechanicDTO[] mechanicsData = ConvertJson(jsonResponseContent);

            return mechanicsData;
        }

        public MechanicDTO GetMechanic(int id)
        {
            string webServiceUrl = $"{protocol}://localhost:{port}/MechanicsData/GetMechanic?id={id}";

            Task<string> webServiceCall = CallWebService(HttpMethod.Get, webServiceUrl);

            webServiceCall.Wait();

            string jsonResponseContent = webServiceCall.Result;

            MechanicDTO mechanicsData = ConvertJsonObject(jsonResponseContent);

            return mechanicsData;
        }

        public int GetMechanicId()
        {
            string webServiceUrl = $"{protocol}://localhost:{port}/MechanicsData/GetMechanicId";

            Task<string> webServiceCall = CallWebService(HttpMethod.Get, webServiceUrl);

            webServiceCall.Wait();

            string jsonResponseContent = webServiceCall.Result;

            int mechanicsData = int.Parse(jsonResponseContent);

            return mechanicsData;
        }

        private async Task<string> CallWebService(HttpMethod httpMethod, string webServiceUrl)
        {
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(httpMethod, webServiceUrl);

            httpClient.DefaultRequestHeaders.Add("Accept", "application/json");

            HttpResponseMessage httpResponseMessage = await httpClient.SendAsync(httpRequestMessage);

            httpResponseMessage.EnsureSuccessStatusCode();

            string httpResponseContent = await httpResponseMessage.Content.ReadAsStringAsync();

            return httpResponseContent;
        }
        private MechanicDTO[] ConvertJson(string json)
        {
            var options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
            MechanicDTO[] mechanicsData = JsonSerializer.Deserialize<MechanicDTO[]>(json, options);

            return mechanicsData;
        }
        private MechanicDTO ConvertJsonObject(string json)
        {
            var options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
            MechanicDTO mechanicsData = JsonSerializer.Deserialize<MechanicDTO>(json, options);

            return mechanicsData;
        }
    }
}