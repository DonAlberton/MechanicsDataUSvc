﻿using System.Text.Json;

using Masd.EAutoService.MechanicsDataUSvc.Model;
using Masd.EAutoService.MechanicsDataUSvc.Rest.Model;

namespace Masd.EAutoService.MechanicsDataUSvc.Logic
{
    public class MechanicDataFileHandler
    {
        public static MechanicDTO[] ReadMechanicsData(string fileName)
        {
            MechanicDTO[] mechanicsDTO = JsonSerializer.Deserialize<MechanicDTO[]>(File.ReadAllText(fileName));

            return mechanicsDTO;
        }
        public static Mechanic[] ReadMechanics(string fileName)
        {
            Mechanic[] mechanics = ReadMechanicsData(fileName).Select(mechanicsData => new Mechanic(mechanicsData.Id, mechanicsData.Name, mechanicsData.Surname)).ToArray();

            return mechanics;
        }
    }
}
