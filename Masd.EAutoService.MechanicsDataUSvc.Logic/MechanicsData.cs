﻿using Masd.EAutoService.MechanicsDataUSvc.Model;

namespace Masd.EAutoService.MechanicsDataUSvc.Logic
{
    public class MechanicsData : IMechanicsData
    {
        private static readonly object MechanicLock = new object();
        private static readonly Mechanic[] Mechanics;

        static MechanicsData()
        {
            var mechanicReader = new MechanicDataFileHandler();
            lock (MechanicLock)
            {
                Mechanic[] mechanics = MechanicDataFileHandler.ReadMechanics("mechanics.json");
                Mechanics = mechanics;
            }
        }

        

        public Mechanic[] GetMechanics()
        {
            lock (MechanicLock)
            {
                return Mechanics;
            }
        }

        public Mechanic GetMechanic(int id)
        {
            lock (MechanicLock)
            {
                return Mechanics.FirstOrDefault(m => m.Id == id);
            }

        }

        public int GetMechanicId()
        {
            lock (MechanicLock)
            {
                return Mechanics.Last().Id;
            }

        }
    }
}