using Masd.EAutoService.MechanicsDataUSvc.Logic;
using Masd.EAutoService.MechanicsDataUSvc.Model;
using Microsoft.AspNetCore.Mvc;

namespace Masd.EAutoService.MechanicsDataUSvc.Controllers
{
    [ApiController]
    [Route("[Controller]")]
    public class MechanicsDataController : ControllerBase, IMechanicsData
    {
        private readonly ILogger<MechanicsDataController> logger;
        private readonly IMechanicsData mechanicRepo;

        public MechanicsDataController(ILogger<MechanicsDataController> logger)
        {
            this.logger = logger;
            mechanicRepo = new MechanicsData();
        }

        [HttpGet]
        [Route("GetMechanics")]
        public Mechanic[] GetMechanics()
        {
            return mechanicRepo.GetMechanics();
        }

        [HttpGet]
        [Route("GetMechanic")]
        public Mechanic GetMechanic(int id)
        {
            return mechanicRepo.GetMechanic(id);
        }

        [HttpGet]
        [Route("GetMechanicId")]
        public int GetMechanicId()
        {
            return mechanicRepo.GetMechanicId();
        }
    }
}