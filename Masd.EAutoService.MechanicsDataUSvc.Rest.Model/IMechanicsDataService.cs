﻿namespace Masd.EAutoService.MechanicsDataUSvc.Rest.Model
{
    public interface IMechanicsDataService
    {
        public MechanicDTO[] GetMechanics();
        public MechanicDTO GetMechanic(int id);

        public int GetMechanicId();
    }
}
