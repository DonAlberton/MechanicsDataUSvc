﻿namespace Masd.EAutoService.MechanicsDataUSvc.Model
{
    public interface IMechanicsData
    { 
        public Mechanic[] GetMechanics();
        public Mechanic GetMechanic(int id);

        public int GetMechanicId();
    }
}
