﻿namespace Masd.EAutoService.MechanicsDataUSvc.Model
{
    using System;
    public class Mechanic
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }


        public Mechanic(int mechanicId, string mechanicName, string mechanicSurname)
        {
            Id = mechanicId;
            Name = mechanicName;
            Surname = mechanicSurname;
        }
    }
}